//
//  DBMannager.swift
//  db-api
//
//  Created by Edgar Froylan Rodriguez Mondragon on 26/04/18.
//

import GRDB

public class DBMannager {
    
    var dbPath:String?

    public init() {
        
    }
    
    func setUpDB(db_name:String) {
        do {
            let dbQueue = try DatabaseQueue(path: Bundle.main.path(forResource: db_name, ofType: ".sqlite")!)
            _ = try dbQueue.inDatabase{ db in
                try Row.fetchAll(db, "SELECT * FROM parameters")
            }
        } catch {
            print(error)
        }
    }
    
}


# db-api

[![CI Status](https://img.shields.io/travis/Edgar Froylan Rodriguez Mondragon/db-api.svg?style=flat)](https://travis-ci.org/Edgar Froylan Rodriguez Mondragon/db-api)
[![Version](https://img.shields.io/cocoapods/v/db-api.svg?style=flat)](https://cocoapods.org/pods/db-api)
[![License](https://img.shields.io/cocoapods/l/db-api.svg?style=flat)](https://cocoapods.org/pods/db-api)
[![Platform](https://img.shields.io/cocoapods/p/db-api.svg?style=flat)](https://cocoapods.org/pods/db-api)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

db-api is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'db-api'
```

## Author

Edgar Froylan Rodriguez Mondragon, edddea@gmail.com

## License

db-api is available under the MIT license. See the LICENSE file for more info.
